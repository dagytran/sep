package cz.cvut.fel.tranaduc.Repository;

import cz.cvut.fel.tranaduc.Entity.ChangeRequest;
import cz.cvut.fel.tranaduc.Entity.ChangeRequestStateEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

/**
 * @author Tran Anh Duc <dagytran@gmail.com>
 */
@Repository
public interface ChangeRequestRepository extends JpaRepository<ChangeRequest, BigInteger> {
	List<ChangeRequest> findByState(ChangeRequestStateEnum state);
}
