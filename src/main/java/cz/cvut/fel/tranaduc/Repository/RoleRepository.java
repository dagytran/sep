package cz.cvut.fel.tranaduc.Repository;

import cz.cvut.fel.tranaduc.Entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Tran Anh Duc <dagytran@gmail.com>
 */
public interface RoleRepository extends JpaRepository<Role, Long> {
}
