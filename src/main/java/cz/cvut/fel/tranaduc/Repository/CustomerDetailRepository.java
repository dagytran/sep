package cz.cvut.fel.tranaduc.Repository;

import cz.cvut.fel.tranaduc.Entity.CustomerDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

/**
 * @author Tran Anh Duc <dagytran@gmail.com>
 */
@Repository
public interface CustomerDetailRepository extends JpaRepository<CustomerDetail, BigInteger>{
}
