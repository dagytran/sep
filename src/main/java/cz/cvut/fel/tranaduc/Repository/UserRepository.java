package cz.cvut.fel.tranaduc.Repository;

import cz.cvut.fel.tranaduc.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Tran Anh Duc <dagytran@gmail.com>
 */
public interface UserRepository extends JpaRepository<User, Long> {
	User findByUsername(String username);
}
