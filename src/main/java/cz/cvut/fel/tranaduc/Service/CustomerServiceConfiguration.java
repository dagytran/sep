package cz.cvut.fel.tranaduc.Service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

/**
 * @author Tran Anh Duc <dagytran@gmail.com>
 */

@Configuration
public class CustomerServiceConfiguration {

	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath("cz.profinit.customerdatabase");
		return marshaller;
	}

	@Bean
	public CustomerService customerService(Jaxb2Marshaller marshaller) {
		CustomerService client = new CustomerService();
		client.setDefaultUri(CustomerService.getWsUrl());
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}
}
