package cz.cvut.fel.tranaduc.Service;

import cz.cvut.fel.tranaduc.Entity.*;
import cz.cvut.fel.tranaduc.Repository.ChangeRequestRepository;
import cz.profinit.customerdatabase.CreateCustomerChangeOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

/**
 * @author Tran Anh Duc <dagytran@gmail.com>
 */
@Service
public class ChangeRequestService extends WebServiceGatewaySupport {

	private ChangeRequestRepository changeRequestRepository;

	@Autowired
	public ChangeRequestService (ChangeRequestRepository changeRequestRepository) {
		this.changeRequestRepository = changeRequestRepository;
	}

	public List<ChangeRequest> getAll() {
		return changeRequestRepository.findAll();
	}

	public List<ChangeRequest> getAllPending() {
		return changeRequestRepository.findByState(ChangeRequestStateEnum.PENDING);
	}

	public List<ChangeRequest> getAllSent() {
		return changeRequestRepository.findByState(ChangeRequestStateEnum.SENT);
	}

	public List<ChangeRequest> getAllDeleted() {
		return changeRequestRepository.findByState(ChangeRequestStateEnum.DELETED);
	}

	public List<ChangeRequest> create(ChangeRequest changeRequest) {
		changeRequestRepository.save(changeRequest);
		return changeRequestRepository.findAll();
	}

	public List<ChangeRequest> delete(BigInteger id) {
		ChangeRequest temp = changeRequestRepository.findOne(id);
		temp.setState(ChangeRequestStateEnum.DELETED)
			.setDateUpdated(new Date());
		changeRequestRepository.save(temp);

		return changeRequestRepository.findAll();
	}

	public void sendPendingRequests() {
		List<ChangeRequest> changeRequests = changeRequestRepository.findByState(ChangeRequestStateEnum.PENDING);
		changeRequests.forEach(changeRequest -> {
			// SEND to API
			sendCustomerChangeOrder(changeRequest);
			// Update in DB
			changeRequest.setDateUpdated(new Date()).setState(ChangeRequestStateEnum.SENT);
			changeRequestRepository.save(changeRequest);
		});
	}

	public void save(ChangeRequest changeRequest) {
		changeRequestRepository.save(changeRequest);
	}

	public void customerAction(CustomerDetail customerDetail, String action ) {
		ChangeRequest changeRequest = new ChangeRequest(customerDetail, action);
		changeRequestRepository.save(changeRequest);
	}

	private void sendCustomerChangeOrder(ChangeRequest changeRequest) {
		CreateCustomerChangeOrder request = new CreateCustomerChangeOrder();

		request.setId(changeRequest.getId());
		request.setCustomer(changeRequest.getCustomer().toCustomerDetailType());
		request.setRequestType(changeRequest.getRequestType());
		try {
			getWebServiceTemplate().marshalSendAndReceive(
					CustomerService.getWsUrl(),
					request,
					new SoapActionCallback(CustomerService.getWsUrl().concat("/CreateCustomerChangeOrder"))
			);
		} catch (IllegalStateException ex) {
			System.out.printf("Request with id : % d could not be sent.", changeRequest.getId());
			System.out.println(ex.getMessage());
		}
	}
}
