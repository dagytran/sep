package cz.cvut.fel.tranaduc.Service;

import cz.cvut.fel.tranaduc.Entity.User;

/**
 * @author Tran Anh Duc <dagytran@gmail.com>
 */
public interface UserService {
	void save(User user);

	User findByUsername(String username);
}
