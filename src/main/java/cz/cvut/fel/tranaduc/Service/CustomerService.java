package cz.cvut.fel.tranaduc.Service;

import cz.cvut.fel.tranaduc.Entity.CustomerDetail;
import cz.profinit.customerdatabase.*;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import java.math.BigInteger;
import java.util.List;

/**
 * @author Tran Anh Duc <dagytran@gmail.com>
 */

public class CustomerService extends WebServiceGatewaySupport {

	private static String WS_URL;

	public static String getWsUrl() {
		return WS_URL;
	}

	public static void setWsUrl(String wsUrl) {
		WS_URL = wsUrl;
	}

	public CustomerDetail getCustomerById(BigInteger id) throws Exception {
		ReadCustomerDetails request = new ReadCustomerDetails();
		request.setId(id);
		ReadCustomerDetailsResponse1 response = (ReadCustomerDetailsResponse1) getWebServiceTemplate()
				.marshalSendAndReceive(
						WS_URL,
						request,
						new SoapActionCallback(WS_URL.concat("/ReadCustomerDetails"))
				);
		CustomerDetailType customerDetailType = response.getCustomer();
		CustomerDetail customerDetail = new CustomerDetail(customerDetailType);
		customerDetail.setUserId(id);
		return customerDetail;
	}

	public List<CustomerType> findCustomers(BigInteger from, BigInteger count) throws Exception {
		ReadAllCustomers request = new ReadAllCustomers();
		request.setFrom(from);
		request.setCount(count);

		ReadAllCustomersResponse1 response = (ReadAllCustomersResponse1) getWebServiceTemplate()
				.marshalSendAndReceive(
						WS_URL,
						request,
						new SoapActionCallback(WS_URL.concat("/ReadAllCustomers"))
				);

		return response.getCustomer();
	}

}
