package cz.cvut.fel.tranaduc.Service;

/**
 * @author Tran Anh Duc <dagytran@gmail.com>
 */
public interface SecurityService {
	String findLoggedInUsername();

	void autologin(String username, String password);
}
