package cz.cvut.fel.tranaduc.Entity;

import cz.profinit.customerdatabase.CreateCustomerChangeOrder;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;

/**
 * @author Tran Anh Duc <dagytran@gmail.com>
 */
@Entity
public class ChangeRequest {

	@Id
	@GeneratedValue
	private BigInteger id;

	@ManyToOne(cascade = CascadeType.ALL)
	private CustomerDetail customer;

	private String requestType;

	private Enum state;

	private Date dateCreated;

	private Date dateUpdated;

	private Date dateProcessed;

	public ChangeRequest() {
		this.dateCreated = new Date();
		this.dateUpdated = new Date();
		state = ChangeRequestStateEnum.PENDING;
	}

	public ChangeRequest(CustomerDetail customer, String requestType) {
		this.customer = customer;
		this.dateCreated = new Date();
		this.dateUpdated = new Date();
		this.requestType = requestType;
		state = ChangeRequestStateEnum.PENDING;
	}

	public BigInteger getId() {
		return id;
	}

	public CustomerDetail getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDetail customer) {
		this.customer = customer;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public Enum getState() {
		return state;
	}

	public ChangeRequest setState(Enum state) {
		this.state = state;
		return this;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public ChangeRequest setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
		return this;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public ChangeRequest setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
		return this;
	}

	public Date getDateProcessed() {
		return dateProcessed;
	}

	public ChangeRequest setDateProcessed(Date dateProcessed) {
		this.dateProcessed = dateProcessed;
		return this;
	}

	public CreateCustomerChangeOrder toCreateCustomerChangeOrder(String requestType) {
		CreateCustomerChangeOrder customerChangeOrder = new CreateCustomerChangeOrder();
		customerChangeOrder.setId(this.id);
		customerChangeOrder.setCustomer(this.customer.toCustomerDetailType());
		customerChangeOrder.setRequestType(requestType);
		return customerChangeOrder;
	}

	@Override
	public String toString() {
		return "Změnový požadavek č. " + id +
				"Klient : " + customer +
				", Typ požadavku: " + requestType +
				", Stav požadavku " + state +
				", Datum vytvoření: " + dateCreated +
				'}';
	}
}
