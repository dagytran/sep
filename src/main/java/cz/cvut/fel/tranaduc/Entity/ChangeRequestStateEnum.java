package cz.cvut.fel.tranaduc.Entity;

public enum ChangeRequestStateEnum {
	SENT, PENDING, DELETED
}