package cz.cvut.fel.tranaduc.Entity;

import cz.profinit.customerdatabase.AddressType;
import cz.profinit.customerdatabase.CustomerDetailType;
import cz.profinit.customerdatabase.PhoneType;

import java.util.Arrays;
import java.util.List;

/**
 * @author Tran Anh Duc <dagytran@gmail.com>
 */
public class CustomerDetailTypeEditable extends CustomerDetailType {

	public void setFirstName (String firstName) {
		super.firstName = Arrays.asList(firstName.split(" "));
	}

	public void setSurname(String surname) {
		super.surname = Arrays.asList(surname.split(" "));
	}

	public void setAddress(List<AddressType> addresses) {
		super.address = addresses;
	}

	public void setPhoneNumber(List<PhoneType> phoneType) {
		super.phoneNum = phoneType;
	}

	public void setCountryOfOrigin(String countryOfOrigin) {
		super.countryOfOrigin = countryOfOrigin;
	}

}
