package cz.cvut.fel.tranaduc.Entity;

import cz.profinit.customerdatabase.CustomerType;

import javax.persistence.*;
import java.math.BigInteger;

/**
 * @author Tran Anh Duc <dagytran@gmail.com>
 */
@Entity
@Table(name = "customer")
public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	protected BigInteger id;

	protected String firstName;

	protected String surname;

	protected String status;

	public Customer() {
	}

	public Customer(BigInteger id, String firstName, String surname, String status) {
		this.id = id;
		this.firstName = firstName;
		this.surname = surname;
		this.status = status;
	}

	public Customer(String id, String firstName, String surname, String status) {
		this.id = new BigInteger(id);
		this.firstName = firstName;
		this.surname = surname;
		this.status = status;
	}

	public Customer(CustomerType customerType) {
		this.id = customerType.getId();
		this.firstName = customerType.getFirstName();
		this.surname = customerType.getSurname();
		this.status = customerType.getStatus();
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Customer{" +
				"id=" + id +
				", firstname='" + firstName + '\'' +
				", surname='" + surname + '\'' +
				", status='" + status + '\'' +
				'}';
	}
}
