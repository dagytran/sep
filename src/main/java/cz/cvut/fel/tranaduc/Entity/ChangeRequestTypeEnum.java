package cz.cvut.fel.tranaduc.Entity;

public enum ChangeRequestTypeEnum {
	CREATE, CHANGE, SUSPEND, REFUND, DEACTIVATE
}
