package cz.cvut.fel.tranaduc.Entity;

import cz.profinit.customerdatabase.AddressType;
import cz.profinit.customerdatabase.CustomerDetailType;
import cz.profinit.customerdatabase.PhoneType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigInteger;
import java.util.List;

/**
 * @author Tran Anh Duc <dagytran@gmail.com>
 */
@Entity
@Table(name = "customer_detail")
public class CustomerDetail {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private BigInteger id;
	private BigInteger userId;

	@NotNull @Size(min=2, max=30)
	private String firstname;
	@NotNull
	@Size(min=2, max=30)
	private String surname;

	// Address


	@NotNull
	@Size(min=2, max=30)
	private  String streetName = "";

	@NotNull
	@Size(min=1, max=30)
	private  String streetNum = "";

	@NotNull
	@Size(min=5, max=6)
	private  String postalCode = "";

	@NotNull
	@Size(min=2, max=30)
	private  String cityPart = "";

	@NotNull
	@Size(min=2, max=30)
	private  String city = "";

	@NotNull
	@Size(min=2, max=30)
	private  String county = "";

	@NotNull
	@Size(min=2, max=30)
	private  String country = "";

	// PhoneNumber
	private String phoneNumber = "";
	private BigInteger phoneNumberType;
	private String cityCode = "";
	private String countryCode = "";

	@NotNull @Size(min=6, max=30)
	private String birthNum;

	private String countryOfOrigin;

	@Transient
	private List<AddressType> address;

	@Transient
	private List<PhoneType> phoneTypes;

	public CustomerDetail() {}

	public CustomerDetail(BigInteger userId, String firstname, String surname) {
		this.userId = userId;
		this.firstname = firstname;
		this.surname = surname;
	}

	public CustomerDetail(
			BigInteger userId,
			String firstName,
			String surname,
			String streetName,
			String streetNum,
			String postalCode,
			String cityPart,
			String city,
			String county,
			String country,
			String phoneNumber,
			BigInteger phoneNumberType,
			String cityCode,
			String countryCode,
			String birthNum,
			String countryOfOrigin) {
		this.userId = userId;
		this.firstname = firstName;
		this.surname = surname;
		this.streetName = streetName;
		this.streetNum = streetNum;
		this.postalCode = postalCode;
		this.cityPart = cityPart;
		this.city = city;
		this.county = county;
		this.country = country;
		this.phoneNumber = phoneNumber;
		this.phoneNumberType = phoneNumberType;
		this.cityCode = cityCode;
		this.countryCode = countryCode;
		this.birthNum = birthNum;
		this.countryOfOrigin = countryOfOrigin;
	}

	public CustomerDetail(CustomerDetailType customerDetailType) {
		this.firstname = nameToString(customerDetailType.getFirstName());
		this.surname = nameToString(customerDetailType.getSurname());
		setAddress(customerDetailType.getAddress());
		setPhoneNumber(customerDetailType.getPhoneNum());
		this.birthNum = customerDetailType.getBirthNum();
		this.countryOfOrigin = customerDetailType.getCountryOfOrigin();
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigInteger getUserId() {
		return userId;
	}

	public void setUserId(BigInteger userId) {
		this.userId = userId;
	}

	public void setAddress(List<AddressType> addresses) {
		this.address = addresses;
		for (AddressType address : addresses) {
			this.streetName = streetName.concat(address.getStreetName());
			this.streetNum = streetNum.concat(address.getStreetNum());
			this.postalCode = postalCode.concat(address.getPostalCode());
			this.cityPart = cityPart.concat(address.getCityPart());
			this.city = city.concat(address.getCity());
			this.county = county.concat(address.getCounty());
			this.country = country.concat(address.getCounty());
		}
	}

	public void setPhoneNumber(List<PhoneType> phoneTypes) {
		this.phoneTypes = phoneTypes;
		for (PhoneType phoneType : phoneTypes) {
			this.phoneNumberType = phoneType.getPhoneNumberType();
			this.phoneNumber = phoneNumber.concat(phoneType.getPhoneNum());
			this.cityCode = cityCode.concat(phoneType.getCityCode());
			this.countryCode = countryCode.concat(phoneType.getCountryCode());
		}
	}

	public String phoneNumberToString() {
		return this.phoneNumberType.toString().concat("__")
				.concat(this.phoneNumber).concat("__")
				.concat(this.cityCode).concat("__")
				.concat(this.countryCode);

	}

	private static String nameToString(List<String> names) {
		StringBuilder sb = new StringBuilder();
		for (String name : names) {
			sb.append(name);
		}
		return sb.toString();
	}

	public CustomerDetailType toCustomerDetailType() {
		CustomerDetailTypeEditable customerDetailType = new CustomerDetailTypeEditable();
		customerDetailType.setFirstName(this.firstname);
		customerDetailType.setSurname(this.surname);
		customerDetailType.setAddress(this.address);
		customerDetailType.setPhoneNumber(this.phoneTypes);
		customerDetailType.setBirthNum(this.birthNum);
		customerDetailType.setCountryOfOrigin(this.countryOfOrigin);
		return customerDetailType;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public void setStreetNum(String streetNum) {
		this.streetNum = streetNum;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public void setCityPart(String cityPart) {
		this.cityPart = cityPart;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setPhoneNumberType(BigInteger phoneNumberType) {
		this.phoneNumberType = phoneNumberType;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public void setBirthNum(String birthNum) {
		this.birthNum = birthNum;
	}

	public void setCountryOfOrigin(String countryOfOrigin) {
		this.countryOfOrigin = countryOfOrigin;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getSurname() {
		return surname;
	}

	public String getStreetName() {
		return streetName;
	}

	public String getStreetNum() {
		return streetNum;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public String getCityPart() {
		return cityPart;
	}

	public String getCity() {
		return city;
	}

	public String getCounty() {
		return county;
	}

	public String getCountry() {
		return country;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public BigInteger getPhoneNumberType() {
		return phoneNumberType;
	}

	public String getCityCode() {
		return cityCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public String getBirthNum() {
		return birthNum;
	}

	public String getCountryOfOrigin() {
		return countryOfOrigin;
	}

	public List<AddressType> getAddress() {
		return address;
	}

	public List<PhoneType> getPhoneTypes() {
		return phoneTypes;
	}

	@Override
	public String toString() {

		return "CustomerDetail{" +
				"id=" + userId + (firstname != null && surname != null ?
				", Jméno: '" + firstname + '\''  +
				", Příjmení: ='" + surname : "")  +  "\'}";
	}
}
