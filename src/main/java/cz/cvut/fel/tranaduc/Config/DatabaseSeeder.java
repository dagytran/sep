package cz.cvut.fel.tranaduc.Config;

import cz.cvut.fel.tranaduc.Entity.User;
import cz.cvut.fel.tranaduc.Repository.UserRepository;
import cz.cvut.fel.tranaduc.Service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * @author Tran Anh Duc <dagytran@gmail.com>
 */

@Component
public class DatabaseSeeder implements CommandLineRunner {

	private UserRepository userRepository;

	@Autowired
	public DatabaseSeeder(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public void run(String... strings) throws Exception {
		// Setting up webservice
		if (strings.length > 0) {
			CustomerService.setWsUrl(strings[0]);
		} else {
			CustomerService.setWsUrl("http://localhost:8088/mockCustomerDatabaseSOAP");
		}

		System.out.println("Inserting users");

		userRepository.save(new User("admin", "password"));
		userRepository.save(new User("Jon Doe", "password"));

	}
}
