package cz.cvut.fel.tranaduc.Controller;

import cz.cvut.fel.tranaduc.Entity.ChangeRequest;
import cz.cvut.fel.tranaduc.Service.ChangeRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;

/**
 * @author Tran Anh Duc <dagytran@gmail.com>
 */
@RestController
@RequestMapping(value = "/change-request")
public class ChangeRequestController {

	private ChangeRequestService changeRequestService;

	@Autowired
	public ChangeRequestController (ChangeRequestService changeRequestService) {
		this.changeRequestService = changeRequestService;
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	public List<ChangeRequest> getAllRequests() {
		return changeRequestService.getAll();
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<ChangeRequest> getRequests() {
		return changeRequestService.getAll();
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<ChangeRequest> getAll() {
		return changeRequestService.getAll();
	}

	@RequestMapping(value = "/pending", method = RequestMethod.GET)
	public List<ChangeRequest> getAllPending() {
		return changeRequestService.getAllPending();
	}

	@RequestMapping(value = "/sent", method = RequestMethod.GET)
	public List<ChangeRequest> getAllSent() {
		return changeRequestService.getAllSent();
	}

	@RequestMapping(value = "/deleted", method = RequestMethod.GET)
	public List<ChangeRequest> getAllDeleted() {
		return changeRequestService.getAllDeleted();
	}


	@RequestMapping(value = "/", method = RequestMethod.POST)
	public List<ChangeRequest> create(@RequestBody ChangeRequest changeRequest) {
		changeRequestService.create(changeRequest);

		return changeRequestService.getAll();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public List<ChangeRequest> delete(@PathVariable BigInteger id) {

		changeRequestService.delete(id);

		return changeRequestService.getAll();
	}

	@RequestMapping(value = "/pending/send/all", method = RequestMethod.GET)
	public String sendPending() {
		changeRequestService.sendPendingRequests();
		return "ok";
	}

}
