package cz.cvut.fel.tranaduc.Controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserController {

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model, String error, String logout) {
		if (error != null)
			model.addAttribute("error", "Zadali jste neplatné uživatelské jméno nebo heslo");

		if (logout != null)
			model.addAttribute("message", "Přihlášení bylo úspěšné. :)");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.isAuthenticated() && !auth.getName().equals("anonymousUser")) {
			return "redirect:/index";
		}

		return "login";
	}

}