package cz.cvut.fel.tranaduc.Controller;

import cz.cvut.fel.tranaduc.Entity.ChangeRequest;
import cz.cvut.fel.tranaduc.Entity.CustomerDetail;
import cz.cvut.fel.tranaduc.Service.ChangeRequestService;
import cz.cvut.fel.tranaduc.Service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Tran Anh Duc <dagytran@gmail.com>
 */
@Controller
public class AppController {

	private ChangeRequestService changeRequestService;

	private CustomerService customerService;

	@Autowired
	public AppController(ChangeRequestService changeRequestService, CustomerService customerService) {
		this.changeRequestService = changeRequestService;
		this.customerService = customerService;
	}

	@RequestMapping(value = {"/", "/index"})
	public String index(Model model,
	                    @RequestParam(value = "from", defaultValue = "1") BigInteger from,
	                    @RequestParam(value = "count", defaultValue = "10") BigInteger count
	) throws Exception {
		Map<String, Object> modelData = new HashMap<>();
		modelData.put("customerList", customerService.findCustomers(from, count));
		modelData.put("requestList", changeRequestService.getAllPending());
		modelData.put("from", from);
		modelData.put("count", count);
		model.addAllAttributes(modelData);

		return "index";
	}

	@RequestMapping("/klient/{id}")
	public String customerDetailForm(Model model, @PathVariable BigInteger id) throws Exception {
		try {
			model.addAttribute("customerDetail", customerService.getCustomerById(id));
			return "customer_detail";
		} catch (Exception e) {
			return "redirect:/404";
		}
	}

	@PostMapping("/klient/submit")
	public String customerDetailFormSubmit(@Valid CustomerDetail customerDetail, BindingResult bindingResult, @RequestParam String action) {
		if (bindingResult.hasErrors()) {
			return "customer_detail";
		}
		changeRequestService.customerAction(customerDetail, action);

		return "redirect:/index";
	}

	@RequestMapping("/klient/novy")
	public String newCustomerDetailForm(Model model) throws Exception {

		model.addAttribute("customerDetail", new CustomerDetail());
		return "new_customer_detail";
	}

	@PostMapping("/klient/novy/ulozit")
	public String newCustomerDetailFormSubmit(@Valid CustomerDetail customerDetail, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return "new_customer_detail";
		}
		ChangeRequest changeRequest = new ChangeRequest(customerDetail, "create");
		changeRequestService.save(changeRequest);

		return "redirect:/index";
	}

	@RequestMapping(value = "/request/{id}", method = RequestMethod.DELETE, produces = "application/json")
	public
	@ResponseBody
	String deleteChangeRequest(@PathVariable BigInteger id) {
		changeRequestService.delete(id);
		return "{\"ok\": true}";
	}

	@RequestMapping(value = "/requests/sendall", method = RequestMethod.GET, produces = "application/json")
	public
	@ResponseBody
	String sendPendingRequestsHook() {
		changeRequestService.sendPendingRequests();
		return "{\"ok\": true}";
	}

}
