(function ($) {
	$(function () {
		// Edit page
		$('.j-edit-button-container').hide();


		$('.j-edit').click(function () {
			$('.j-detail-button-container').hide(100, function () {
				$('.j-edit-button-container').show(100)
			});
			$('#j-form').find('input').each(function (index, input) {
				if ($(input).attr('id') != 'id') {
					$(input).prop('disabled', false);
				}
			});
		});
		$('.j-cancel').click(function () {
			$('.j-edit-button-container').hide(100, function () {
				$('.j-detail-button-container').show(100);
			});
			$('#j-form').find('input').each(function (index, input) {
				$(input).prop('disabled', true);
			});
		});

		// Index page

		$('.j-delete-request').click(function (ev) {
			var requestId = $(ev.target).data("id");
			$.ajax({
				url: "/request/" + requestId,
				method: "DELETE"
			}).always(function () {
				document.location.reload();
			});
		});

		$('.j-send-all').click(function () {
			$.ajax({
				url: "/requests/sendall",
				method: "GET"
			}).always(function () {
				document.location.reload();
			});
		});

		// Pagination
		$('.j-next-page').click(function (ev) {
			ev.preventDefault();
			redirectToNextPage($(ev.currentTarget).data("from"), $(ev.currentTarget).data("count"), true);
		});

		$('.j-previous-page').click(function (ev) {
			ev.preventDefault();
			redirectToNextPage($(ev.currentTarget).data("from"), $(ev.currentTarget).data("count"), false);
		});

		function redirectToNextPage(from, count, isNext) {
			var newUrl = window.location.href;
			var newFrom;

			if (isNext) {
				newFrom = from + count;
			} else {
				newFrom = ((from - count) > 0 ? (from - count) : 1)
			}

			if (newUrl.indexOf("from=" + from) > -1) {
				newUrl = newUrl.replace("from=" + from, "from=" + newFrom);
			} else {
				newUrl += "?from=" + newFrom + "&count=" + count;
			}
			window.location.href = newUrl;
		}

	}); // end of document ready
})(jQuery); // end of jQuery name space
